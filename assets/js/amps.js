var ussd_content = document.getElementById("ussd_content"); //ussd container
var next_btn = document.getElementById("next_btn"); //affirmation button
var cancel_btn = document.getElementById("cancel_btn"); //cancel button
var opt_in = document.getElementById("opt_in");
var opt_out = document.getElementById("opt_out");
var opt_out_btn = document.getElementById('switch');
var dial_pad = document.getElementById('dial_pad');
var dial_txtbox = document.getElementById('dial_txtbox');
var progress_counter = 0; //stage counter
var recharge_amount = 0;

//counter that keeps track of stage in the process
function increment_stage(counter) {
	return counter += 1;
}

//cancel USSD session
function cancel_sess(sess_div){
	sess_div.style.display = "none";

}

//An object that holds all the possible contents the USSD can display
contents_obj = {	
					recharge_amount: 0,

					amount:     '<h4>AMPS Notice</h4><hr />' +
								'<p>Enter amount to recharge:</p>' +
								'<input type="number" id="ussd_txtbox" class="form-control" /><br />',

					banks:      '<h4>AMPS Notice</h4><hr />' +
								'<p>Select your bank:</p>' +
								'<ul>' +
									'<li>1. First Bank</li>' +
									'<li>2. GTBank</li>' +
									'<li>3. Diamond Bank</li>' +
									'<li>4. Debit Card</li>' +
								'</ul>' +
								'<input type="number" id="ussd_txtbox" class="form-control" /><br />',

					last4:      '<h4>AMPS Notice</h4><hr />' +
								'<p>Please enter the last four digits of your debit card:</p>' +
								'<input type="number" id="ussd_txtbox" maxlength="4" class="form-control" /><br />',

					autotop:    function(amt) {
									return  '<h4>AMPS Notice</h4><hr />' +
											'<p>AMPS <b>=N='+ amt +'</b> Top Up Successful! Opt-in to automatic top-up and never run out of airtime?</p>' +
											'<ul>' +
												'<li>1. Yes, OPT-IN</li>' +
												'<li>2. No, EXIT</li>' +
											'</ul>' +
											'<input type="number" id="ussd_txtbox" class="form-control" /><br />'
								},

					confirm:    function(amt) {
									return  '<h4>AMPS Notice</h4><hr />' +
											'<p>You are registering for AMPS Auto-Top service of <b>=N='+ amt +'</b></p>' +
											'<ul>' +
												'<li>1. Confirm</li>' +
												'<li>2. EXIT</li>' +
											'</ul>' +
											'<input type="number" id="ussd_txtbox" class="form-control" /><br />'
								},

					close:      '<h4>AMPS Notice</h4><hr />' +
								'<p>You have successfully subscribed for AMPS Auto Topup service</p>'
				};

//determine the appropriate USSD content to show and display it
function display_ussd(ussd_div, contents) 
{
	if(next_btn.textContent == "Yes") {
		next_btn.textContent = "Send";	
		return ussd_div.innerHTML = contents.amount;
	}

	switch(progress_counter) {
		case 1:
			recharge_amount = ussd_txtbox.value;
			return ussd_div.innerHTML = contents.banks;
			break
		case 2:
			return ussd_div.innerHTML = contents.last4;
			break
		case 3:
			return ussd_div.innerHTML = contents.autotop.call(window, recharge_amount);
			break
		case 4:
			if( ussd_txtbox.value == 2 ) {
				return opt_in.style.display = 'none';
			}
			return ussd_div.innerHTML = contents.confirm.call(window, recharge_amount);
			break
		case 5:
			if( ussd_txtbox.value == 2 ) {
				return opt_in.style.display = 'none';
			}
			next_btn.style.display = "none";
			cancel_btn.textContent = "Ok";
			return ussd_div.innerHTML = contents.close;
			break
	}
}


opt_out_btn.addEventListener('click', function(e) {
	e.preventDefault();
	opt_in.style.display = "none";
	dial_pad.style.display = "block";
})

//attach click event to the affirmation button (YES/SEND)
next_btn.addEventListener('click', function() {
	display_ussd(ussd_content, contents_obj); 
	progress_counter += 1;
	return false;
});

//cancel USSD session...click event for cancel button
cancel_btn.addEventListener('click', function() {
	return cancel_sess(opt_in);
})

//cancel USSD session...click event for can call button
document.getElementById('cancel_call_btn').addEventListener('click', function() {
	return cancel_sess(dial_pad);
})

document.getElementById('call_btn').addEventListener('click', function(e) {
	e.preventDefault();
	if(dial_txtbox.value == ''){
		alert(dial_txtbox.value);
		document.getElementById('alert').style.display = "block";
		return;
	}

	dial_pad.style.display = "none";
	opt_out.style.display = "block";

})

document.getElementById('opt_out_btn').addEventListener('click', function(e){
	e.preventDefault();
	if( document.getElementById('opt_out_ussd_txtbox').value == 2 ) {
		return opt_out.style.display = 'none';
	}
	opt_out.style.display = "none";
	document.getElementById('confirm_out_opt').style.display = "block";
})

document.getElementById('final_opt_outbtn').addEventListener('click', function(e){
	e.preventDefault();
	return cancel_sess(document.getElementById('confirm_out_opt'));
})